package messque

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"github.com/apache/pulsar-client-go/pulsar"
	"go.uber.org/zap"
)

var (
	ErrNotFoundProducer = errors.New("not found producer by topic")
)

func Subscribe(topic string, handler func(msg pulsar.Message)) {
	addHandler(topic, handler)
}

func Send(topic string, v interface{}) error {
	// 先做个类型断言，如果是 ProducerMessage 指针类型，不用再 json 编码了，直接发送
	msg, ok := v.(*pulsar.ProducerMessage)
	if !ok {
		msgb, err := json.Marshal(v)
		if err != nil {
			logger.Error("[messque.Send] marshal producer message fail", zap.NamedError("err", err))
			return err
		}
		msg = &pulsar.ProducerMessage{
			Payload: msgb,
		}
	}
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if producer, ok := getProducer(topic); ok {
		if msgID, err := producer.Send(ctx, msg); err != nil {
			logger.Error("[messque.Send] producer send message to pulsar queue fail", zap.NamedError("err", err), zap.String("topic", topic))
			return err
		} else {
			logger.Info("[messque.Send] producer send message to pulsar queue success", zap.String("topic", topic),
				zap.Int64("msg.entryID", msgID.EntryID()), zap.Int64("msg.ledgerID", msgID.LedgerID()))
		}
	} else {
		logger.Error("[messque.Send] not found producer", zap.String("topic", topic))
		return ErrNotFoundProducer
	}
	return nil
}

func Ack(msg pulsar.Message) {
	if consumer != nil {
		consumer.Ack(msg)
	}
}
