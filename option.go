package messque

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strings"

	jcr "github.com/tinode/jsonco"
	"go.uber.org/zap"
)

type CompressionType int

const (
	NoCompression CompressionType = iota
	LZ4
	ZLib
	ZSTD
)

func (c CompressionType) Marshal() string {
	var t string
	switch c {
	case LZ4:
		t = "LZ4"
	case ZLib:
		t = "ZLIB"
	case ZSTD:
		t = "ZSTD"
	default:
		t = "NO"
	}
	return t
}

func (c *CompressionType) Unmarshal(typ string) {
	t := strings.ToUpper(typ)
	switch t {
	case "LZ4":
		*c = LZ4
	case "ZLIB":
		*c = ZLib
	case "ZSTD":
		*c = ZSTD
	default:
		*c = NoCompression
	}
}

type CompressionLevel int

const (
	// Default compression level
	Default CompressionLevel = iota

	// Faster compression, with lower compression ration
	Faster

	// Higher compression rate, but slower
	Better
)

func (c CompressionLevel) Marshal() string {
	var l string
	switch c {
	case Default:
		l = "Default"
	case Faster:
		l = "Faster"
	case Better:
		l = "Better"
	default:
		l = "Default"
	}
	return l
}

func (c *CompressionLevel) Unmarshal(level string) {
	l := strings.Title(level)
	switch l {
	case "Faster":
		*c = Faster
	case "Better":
		*c = Better
	default:
		*c = Default
	}
}

type ClientOptions struct {
	// Configure the service URL for the Pulsar service.
	// This parameter is required
	URL string `json:"url"`

	// Timeout for the establishment of a TCP connection (default: 5 seconds)
	ConnectionTimeout int `json:"connection_timeout"`

	// Set the operation timeout (default: 30 seconds)
	// Producer-create, subscribe and unsubscribe operations will be retried until this interval, after which the
	// operation will be marked as failed
	OperationTimeout int `json:"operation_timeout"`

	// Max number of connections to a single broker that will kept in the pool. (Default: 1 connection)
	MaxConnectionsPerBroker int `json:"max_connections_per_broker"`
}

type ProducerOptions struct {
	// Topic specify the topic this producer will be publishing on.
	// This argument is required when constructing the producer.
	Topic string `json:"topic"`

	// CompressionType set the compression type for the producer.
	// By default, message payloads are not compressed. Supported compression types are:
	//  - NO
	//  - LZ4
	//  - ZLIB
	//  - ZSTD
	//
	// Note: ZSTD is supported since Pulsar 2.3. Consumers will need to be at least at that
	// release in order to be able to receive messages compressed with ZSTD.
	CompressionType string `json:"compression_type"`

	// Define the desired compression level. Options:
	// - Default
	// - Faster
	// - Better
	CompressionLevel string `json:"compression_level"`
}

// SubscriptionType of subscription supported by Pulsar
type SubscriptionType int

const (
	// Exclusive there can be only 1 consumer on the same topic with the same subscription name
	Exclusive SubscriptionType = iota

	// Shared subscription mode, multiple consumer will be able to use the same subscription name
	// and the messages will be dispatched according to
	// a round-robin rotation between the connected consumers
	Shared

	// Failover subscription mode, multiple consumer will be able to use the same subscription name
	// but only 1 consumer will receive the messages.
	// If that consumer disconnects, one of the other connected consumers will start receiving messages.
	Failover

	// KeyShared subscription mode, multiple consumer will be able to use the same
	// subscription and all messages with the same key will be dispatched to only one consumer
	KeyShared
)

func (s SubscriptionType) Marshal() string {
	var t string
	switch s {
	case Exclusive:
		t = "Exclusive"
	case Shared:
		t = "Shared"
	case Failover:
		t = "Failover"
	case KeyShared:
		t = "KeyShared"
	default:
		t = "Exclusive"
	}
	return t
}

func (s *SubscriptionType) Unmarshal(typ string) {
	t := strings.Title(typ)
	switch t {
	case "Exclusive":
		*s = Exclusive
	case "Shared":
		*s = Shared
	case "Failover":
		*s = Failover
	case "KeyShared":
		*s = KeyShared
	default:
		*s = Shared
	}
}

type ConsumerOptions struct {
	// Specify the topic this consumer will subscribe on.
	// Either a topic, a list of topics or a topics pattern are required when subscribing
	Topic string `json:"topic"`

	// Specify a list of topics this consumer will subscribe on.
	// Either a topic, a list of topics or a topics pattern are required when subscribing
	Topics []string `json:"topics"`

	// Specify a regular expression to subscribe to multiple topics under the same namespace.
	// Either a topic, a list of topics or a topics pattern are required when subscribing
	TopicsPattern string `json:"topics_pattern"`

	// Specify the subscription name for this consumer
	// This argument is required when subscribing
	SubscriptionName string `json:"subscription_name"`

	// Select the subscription type to be used when subscribing to the topic.
	// - Exclusive
	// - Shared
	// - Failover
	// - KeyShared
	// Default is `Exclusive`
	Type string `json:"subscription_type"`
}

type Options struct {
	Client    ClientOptions     `json:"client"`
	Producers []ProducerOptions `json:"producer"`
	Consumer  ConsumerOptions   `json:"consumer"`
}

func (o *Options) Decode(optionfile string) error {
	if file, err := os.Open(optionfile); err != nil {
		logger.Warn("Failed to read optionfile", zap.NamedError("err", err))
		return err
	} else {
		jr := jcr.New(file)
		if err = json.NewDecoder(jr).Decode(o); err != nil {
			switch jerr := err.(type) {
			case *json.UnmarshalTypeError:
				lnum, cnum, _ := jr.LineAndChar(jerr.Offset)
				logger.Fatal("[Options.Decode] unmarshall error in Hotfile", zap.String("field", jerr.Field), zap.Int("line", lnum),
					zap.Int("char", cnum), zap.Int64("offset(bytes)", jerr.Offset), zap.NamedError("err", err))
			case *json.SyntaxError:
				lnum, cnum, _ := jr.LineAndChar(jerr.Offset)
				logger.Fatal("[Options.Decode] syntax error in Hotfile at", zap.Int("line", lnum), zap.Int("char", cnum),
					zap.Int64("offset(bytes)", jerr.Offset), zap.NamedError("err", err))
			default:
				logger.Fatal("[Options.Decode] failed to parse Hotfile", zap.NamedError("err", err))
			}

		}
		file.Close()
		return err
	}
}

func confile(prefix string) string {
	return filepath.Join(prefix, "etc", "conf", "messque.conf")
}
