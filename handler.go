package messque

import (
	"context"

	"github.com/apache/pulsar-client-go/pulsar"
	"go.uber.org/zap"
)

func Receive() {
	for {
		msg, err := consumer.Receive(context.Background())
		if err != nil {
			logger.Error("[messque.Receive] consumer receive message from pulsar queue fail", zap.NamedError("err", err))
			break
		}
		logger.Info("[messque.Receive] consumer receive message from pulsar queue success",
			zap.String("topic", msg.Topic()), zap.Int64("msg.entryID", msg.ID().EntryID()), zap.Int64("msg.ledgerID", msg.ID().LedgerID()))
		go doMessage(msg)
	}
}

func doMessage(msg pulsar.Message) {
	topic := msg.Topic()
	if h, ok := getHandler(topic); ok {
		h(msg)
	} else {
		logger.Error("[messque.doMessage] not found message handler", zap.String("topic", topic),
			zap.Int64("msg.entryID", msg.ID().EntryID()), zap.Int64("msg.ledgerID", msg.ID().LedgerID()))
	}
}
