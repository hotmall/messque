package messque

import (
	"sync"
	"time"

	"github.com/apache/pulsar-client-go/pulsar"
	"github.com/hotmall/commandline"
	"github.com/hotmall/logging"
	"go.uber.org/zap"
)

var (
	once      sync.Once
	logger    *zap.Logger
	consumer  pulsar.Consumer
	producers map[string]pulsar.Producer
	pmutex    sync.RWMutex
	handlers  map[string]func(msg pulsar.Message)
	hmutex    sync.RWMutex
)

func init() {
	once.Do(func() {
		initClient(commandline.PrefixPath())
	})
}

func initClient(prefix string) {
	// 初始化日志
	logger = logging.Logger("messque")

	optionFile := confile(prefix)
	var options Options
	if err := options.Decode(optionFile); err != nil {
		logger.Panic("[messque.initClient] decode options fail", zap.NamedError("err", err), zap.String("optionFile", optionFile))
	}

	co := pulsar.ClientOptions{
		URL:                     options.Client.URL,
		ConnectionTimeout:       time.Duration(options.Client.ConnectionTimeout) * time.Second,
		OperationTimeout:        time.Duration(options.Client.OperationTimeout) * time.Second,
		MaxConnectionsPerBroker: options.Client.MaxConnectionsPerBroker,
	}
	client, err := pulsar.NewClient(co)
	if err != nil {
		logger.Panic("[messque.initClient] could not instantiate Pulsar client", zap.NamedError("err", err))
	}

	handlers = make(map[string]func(msg pulsar.Message))

	producers = make(map[string]pulsar.Producer)
	for _, p := range options.Producers {
		var ct CompressionType
		ct.Unmarshal(p.CompressionType)
		var cl CompressionLevel
		cl.Unmarshal(p.CompressionLevel)
		po := pulsar.ProducerOptions{
			Topic:            p.Topic,
			CompressionType:  pulsar.CompressionType(ct),
			CompressionLevel: pulsar.CompressionLevel(cl),
		}
		logger.Info("[messque.initClient] producer options", zap.String("topic", po.Topic), zap.Int("CompressionType", int(po.CompressionType)),
			zap.Int("CompressionLevel", int(po.CompressionLevel)))
		if producer, err := client.CreateProducer(po); err != nil {
			logger.Panic("[messque.initClient] failed to create producer", zap.NamedError("err", err), zap.String("topic", po.Topic),
				zap.String("CompressionType", p.CompressionType), zap.String("CompressionLevel", p.CompressionLevel))
		} else {
			// producers[p.Topic] = producer
			addProducer(p.Topic, producer)
		}
	}

	var st SubscriptionType
	st.Unmarshal(options.Consumer.Type)
	consumerOpts := pulsar.ConsumerOptions{
		Topic:            options.Consumer.Topic,
		Topics:           options.Consumer.Topics,
		TopicsPattern:    options.Consumer.TopicsPattern,
		SubscriptionName: options.Consumer.SubscriptionName,
		Type:             pulsar.SubscriptionType(st),
	}
	consumer, err = client.Subscribe(consumerOpts)
	if err != nil {
		logger.Panic("[messque.initClient] failed to subscribe pulsar topic", zap.NamedError("err", err))
	}

	go Receive()
}

func addHandler(topic string, h func(msg pulsar.Message)) {
	hmutex.Lock()
	defer hmutex.Unlock()
	handlers[topic] = h
}

func getHandler(topic string) (h func(msg pulsar.Message), ok bool) {
	hmutex.RLock()
	defer hmutex.RUnlock()
	h, ok = handlers[topic]
	return
}

func addProducer(topic string, p pulsar.Producer) {
	pmutex.Lock()
	defer pmutex.Unlock()
	producers[topic] = p
}

func getProducer(topic string) (p pulsar.Producer, ok bool) {
	pmutex.RLock()
	defer pmutex.RUnlock()
	p, ok = producers[topic]
	return
}
